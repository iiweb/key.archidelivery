var spinner;
var spinner_options = {lines:11, length:4, width:3,	radius:9, corners:1, rotate:0, color:'#000', speed:1, trail:60, shadow:false, hwaccel:false, className:'spinner', zIndex:2e9, top:'auto', left: 'auto' };

$(function(){
	$('.alert').alert();
	$('#input').mask('9999-9999-9999-9999').keyup(function(){
		var inputText = $(this).val().replace(/-|_/g,'');
		if(inputText.length==16)
			$(this).siblings('.icon-mask-status').show().removeClass('icon-decline').addClass('icon-accept').parent().parent().removeClass('error');
		else
			$(this).siblings('.icon-mask-status').hide();
	});
	$('#type button').each(function(){
		var button = $(this);
		var hidden = $('#type input');
		button.live('click', function(){
			hidden.val(button.val());
		});
		if(button.val() == hidden.val()) {
			button.addClass('active');
		}
	});
	$('#result span').click(function(){
		var val = $(this).text();
		$(this).hide().siblings('input').show().focus().select().focusout(function(){
			$(this).hide().val(val).siblings('span').show();
		});
	});
	$('#submit').click(function(e){
		e.preventDefault();
		var inputText = $('#input').val().replace(/-|_/g,'');
		if(inputText.length==16)
			sendRequest($('<form><input name="licence" value="'+inputText+'"><input name="type" value="'+$('#type input').val()+'"></form>'));
		else {
			$('#input').parent().parent().addClass('error');
			$('#box').effect('shake',{distance:5});
			$('#error-box').append(
				$('<div class="alert alert-error fade in">Код должен состоять из 16 цифр</div>')
				.css({position:'relative',top:'-20px',opacity:'0'}).animate({top:'0',opacity:'1'}).delay(3000).fadeOut("slow",function(){$(this).remove();})
			);
		}
	});
	$('#retry').click(function(e){
		e.preventDefault();
		$('#input').val('').unmask().mask('9999-9999-9999-9999').siblings('.icon-mask-status').hide().parent().parent().removeClass('error');
		$('#inputs').slideDown();
		$('#result').slideUp();
		$('#submit').show();
		$(this).hide();
	});
});

function sendRequest(form) {
	spinner = new Spinner(spinner_options).spin($('#box-overlay').show().get(0));
	$.ajax({
		type: 'get',
		url: '/license.php',
		data: form.serialize(),
		success: function(data){
			$('#box-overlay').hide();
			spinner.stop();
			if (data=='0') {
				$('#input').parent().parent().addClass('error');
				$('#box').effect('shake',{distance:5});
				$('#error-box').append(
					$('<div class="alert alert-error fade in">Неверный код</div>')
					.css({position:'relative',top:'-20px',opacity:'0'}).animate({top:'0',opacity:'1'}).delay(3000).fadeOut("slow",function(){$(this).remove();})
				);
			} else {
				$('#inputs').slideUp();
				$('#result').slideDown().find('.alert span').text(data).siblings('input').val(data);
				$('#submit').hide();
				$('#retry').show();
			}
		},
		error: function(a,b,c){
			$('#box-overlay').hide();
			spinner.stop();
			alert("Запрос не прошел.\n"+b+': '+c);
		}
	});
}